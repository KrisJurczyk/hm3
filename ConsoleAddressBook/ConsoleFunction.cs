﻿using System;
using System.Collections;
using LibraryAddressBook;

namespace ConsoleAddressBook
{
    internal class ConsoleFunction
    {
        internal static void AvailableFunction()
        {
            Console.WriteLine("Dostępne opcje: ");
            foreach (var actionNames in Enum.GetNames(typeof(EnumOfAction)))
            {
                Console.WriteLine($"- {actionNames}");
            }

            string uch = Console.ReadLine();
            var action = (EnumOfAction)Enum.Parse(typeof(EnumOfAction), uch, true);

            switch (action)
            {
                case EnumOfAction.Add:
                    {
                        Console.Write("[Imie]: ");
                        string a = Console.ReadLine();
                        Console.Write("[Nazwisko]: ");
                        string b = Console.ReadLine();
                        Console.Write("[Email]: ");
                        string c = Console.ReadLine();
                        Console.Write("[Phone]: ");
                        string d = Console.ReadLine();
                        FacadeAddressBook.Add(a, b, c, d);
                        break;
                    }

                case EnumOfAction.Delete:
                    {
                        Console.WriteLine("Wpisz imie i nazwisko osoby do usunięcia:");
                        Console.Write("[Imie]: ");
                        string a = Console.ReadLine();
                        Console.Write("[Nazwisko]: ");
                        string b = Console.ReadLine();
                        FacadeAddressBook.Delete(a, b);
                        break;
                    }

                case EnumOfAction.Select:
                    {
                        Console.WriteLine("Wpisz szukany ciąg znaków:");
                        var y = FacadeAddressBook.Select(Console.ReadLine());
                        foreach (var t in (IEnumerable) y)
                        {
                            Console.WriteLine(t);
                        }
                        break;
                    }

                case EnumOfAction.Show:
                    {
                        var y = FacadeAddressBook.Show();
                        foreach (var t in (IEnumerable) y)
                        {
                            Console.WriteLine(t);
                        }
                        break;
                    }

                default:
                    throw new ArgumentOutOfRangeException(nameof(action), "Brak takiej opcji.");
            }
        }
    }
}