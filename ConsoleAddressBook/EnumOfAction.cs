﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAddressBook
{
    public enum EnumOfAction
    {
        Add,
        Show,
        Select,
        Delete
    }
}
