﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace LibraryAddressBook
{
    public class FacadeAddressBook
    {
        public static void Add(string a, string b, string c, string d)
        {;
            var section = (NameValueCollection) ConfigurationManager.GetSection("AddressBookConfig");
            var z = new Person(a, b, c, d);
            var s = new Person[] {z};
            if (section["Provider"].ToUpper() == "SQL") new PersonSql().Add(a,b,c,d);
            else if (section["Provider"].ToUpper() == "FILE") new PersonFile().Add(s);
        }

        public static object Select(string a)
        {
            var section = (NameValueCollection)ConfigurationManager.GetSection("AddressBookConfig");
            if (section["Provider"].ToUpper() == "SQL") return new PersonSql().Select(a);
            else if (section["Provider"].ToUpper() == "FILE") return new PersonFile().Select(a);
            else throw new ArgumentOutOfRangeException(nameof(Show), "Niepoprawna konfiguracja docelowego pliku");
        }

        public static object Show()
        {
            var section = (NameValueCollection)ConfigurationManager.GetSection("AddressBookConfig");
            if (section["Provider"].ToUpper() == "SQL") return new PersonSql().Load();
            else if (section["Provider"].ToUpper() == "FILE") return new PersonFile().Load();
            else throw new ArgumentOutOfRangeException(nameof(Show), "Niepoprawna konfiguracja docelowego pliku");
        }

        public static void Delete(string a, string b)
        {
            var section = (NameValueCollection)ConfigurationManager.GetSection("AddressBookConfig");
            if (section["Provider"].ToUpper() == "SQL") new PersonSql().Delete(a, b);
            else if (section["Provider"].ToUpper() == "FILE") new PersonFile().Delete();
        }
    }
}
