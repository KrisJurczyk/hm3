﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAddressBook
{
    public class PathProvider
    {
        public NameValueCollection Path { get; }
        public PathProvider()
        {
            var path = (NameValueCollection) ConfigurationManager.GetSection("AddressBookConfig");
            Path = path[key];
        }
    }
}
