﻿using System.Text.RegularExpressions;

namespace LibraryAddressBook
{
    public class Person
    {
        public Person(string a, string b , string c, string d)
        {
            FirstName = a.ToUpper();
            LastName = b.ToUpper();
            Email = c.ToUpper();
            Phone = d.ToUpper();
        }
        public Person(string personString)
        {
            var regex = new Regex("(?<FirstName>.*);(?<LastName>.*);(?<Email>.*);(?<Phone>.*)");
            var match = regex.Match(personString);
            FirstName = match.Groups["FirstName"].Value.ToUpper();
            LastName = match.Groups["LastName"].Value.ToUpper();
            Email = match.Groups["Email"].Value.ToUpper();
            Phone = match.Groups["Phone"].Value.ToUpper();
        }
        
        public string FirstName { get; set; }

        public string LastName { get; set;  }
        
        public string Email { get; set; }
        
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"{FirstName};{LastName};{Email};{Phone}";
        }

        protected bool Equals(Person other)
        {
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName) && string.Equals(Email, other.Email) && string.Equals(Phone, other.Phone);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Person)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Email != null ? Email.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Phone != null ? Phone.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}