﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LibraryAddressBook
{
    public class PersonFile : PathProvider
    {
        public void Add(IEnumerable<Person> persons)
        {
            using (var file = File.Open(Path["ConnectionString"], FileMode.Append, FileAccess.Write))
            {
                using (var s = new StreamWriter(file))
                {
                    foreach (var person in persons.OrderBy(person => person.LastName, StringComparer.OrdinalIgnoreCase))
                    {
                        s.WriteLine(person);
                    }
                }
            }
        }

        public Person[] Load()
        {
            return LazyLoad().OrderBy(person => person.LastName, StringComparer.OrdinalIgnoreCase).ToArray();
        }

        public IEnumerable<Person> LazyLoad()
        {
            using (var file = File.Open(Path["ConnectionString"], FileMode.Open, FileAccess.Read))
            {
                using (var s = new StreamReader(file))
                {
                    string line;
                    while ((line = s.ReadLine()) != null)
                    {
                        yield return new Person(line);
                    }
                }
            }
        }

        public Person[] Select(string a)
        {
            return YSelect(a.ToUpper()).OrderBy(person => person.LastName, StringComparer.OrdinalIgnoreCase).ToArray();
        }

        public IEnumerable<Person> YSelect(string a)
        {
            using (var file = File.Open(Path["ConnectionString"], FileMode.Open, FileAccess.Read))
            {
                using (var s = new StreamReader(file))
                {
                    string line;
                    while ((line = s.ReadLine()) != null)
                    {
                        if (line.Contains($"{a}")) yield return new Person(line);
                        else yield break;
                    }
                }
            }
        }
        //NIEDZIALA
        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}

