﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace LibraryAddressBook
{
    public class PersonSql : PathProvider
    {
        public int Add(string a, string b, string c, string d)
        {
            var person = new Person(a, b, c, d);
            var connect = Path["ConnectionString"];

            using (var connection = new SqlConnection(connect))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("INSERT INTO Persones(firstName,lastName,email,phone) output INSERTED.ID VALUES(@a,@b,@c,@d)", connection))
                {
                    command.Parameters.AddWithValue("@a", person.FirstName);
                    command.Parameters.AddWithValue("@b", person.LastName);
                    command.Parameters.AddWithValue("@c", person.Email);
                    command.Parameters.AddWithValue("@d", person.Phone);

                    int id = (int) command.ExecuteScalar();
                    
                    return id;
                }
            }
        }

        public Person[] Load()
        {
            return YLoad().ToArray();
        }
        private IEnumerable<Person> YLoad()
        {
            var connect = Path["ConnectionString"];

            using (var connection = new SqlConnection(connect))
            {
                connection.Open();

                using (var command = new SqlCommand("SELECT TOP (1000) firstName, lastName, email, phone FROM Persones ORDER BY lastName", connection))
                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        yield return new Person(dataReader.GetValue(0).ToString(), dataReader.GetValue(1).ToString(), dataReader.GetValue(2).ToString(), dataReader.GetValue(3).ToString());
                    }
                }
            }
        }

        public Person[] Select(string a)
        {
            return YSelect(a).ToArray();
        }

        private IEnumerable<Person> YSelect(string a)
        {
            var connect = Path["ConnectionString"];

            using (var connection = new SqlConnection(connect))
            {
                connection.Open();

                using (var command = new SqlCommand($"SELECT firstName, lastName, email, phone FROM Persones WHERE firstName LIKE '%{a}%'" +
                                                    $" OR lastName LIKE '%{a}%' OR email LIKE '%{a}%' OR phone LIKE '%{a}%'", connection))
                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        yield return new Person($"{dataReader.GetValue(0)};{dataReader.GetValue(1)};{dataReader.GetValue(2)};{dataReader.GetValue(3)}");
                    }
                }
            }
        }
        //NIEDZIALA
        public int Delete(string a, string b)
        {
            var connect = Path["ConnectionString"];

            using (var connection = new SqlConnection(connect))
            {
                connection.Open();

                using (var command = new SqlCommand($"DELETE FROM Persones WHERE firstName LIKE %{a.ToUpper()}% AND lastName LIKE %{b.ToUpper()}%", connection))
                {
                    int modified = (int)command.ExecuteScalar();

                    return modified;
                }
            }
        }
    }
}