﻿using NUnit.Framework;
using LibraryAddressBook;
using Moq;

namespace NUnit.Tests
{
    [TestFixture]
    public class UnitTests
    {
        [Test]
        public void Constructor1_String_PropertiesOk()
        {
            var person1 = new Person("Mirek", "Franca","", "321 654 987");

            var act = person1.ToString();

            Assert.That(act, Is.EqualTo("MIREK;FRANCA;;321 654 987"));
        }

        [Test]
        public void Constructor2_String_PropertiesOk()
        {
            var person1 = new Person("Jarek;Ptak;jptak@gmail.com;123 456 789");

            Assert.That(person1.FirstName, Is.EqualTo("JAREK"));
            Assert.That(person1.LastName, Is.EqualTo("PTAK"));
            Assert.That(person1.Email, Is.EqualTo("JPTAK@GMAIL.COM"));
            Assert.That(person1.Phone, Is.EqualTo("123 456 789"));
        }

        [Test]
        public void PersonFile_SaveLoadIntegration_InEqualsOut()
        {
            var customerMock = new Mock<PersonFile>();
            ////NIEDZIA :<
            //customerMock.Setup(x => x.Path.AllKeys[0]).Returns(@"C: \Users\student1\Desktop\C#\HM3\ConsoleAddressBook\ConsoleAddressBook\plik.xml");
            
            var personsArray = new Person[]
            {
                new Person("Mirek", "Franca", "mfranca@gmail.com", "321 654 987"),
                new Person("Jarek;Ptak;jptak@gmail.com;123 456 789"),
                new Person("Janek", "", "jbomba@gmail.com", "111 111 111")
            };

            var personFile = customerMock;

            customerMock.Add(personsArray);
            var personLoad = personFile.Load();

            // Czy ludzie się znajdują w tablicy
            Assert.That(personLoad, Is.EquivalentTo(personsArray));
            // Czy są pięknie posortowani
            Assert.That(personLoad, Is.Ordered.By(nameof(Person.LastName)));
        }
    }
}
